export default {
	target: "static",
	head: {
		title: "open-trivia-db",
		htmlAttrs: {
			lang: "en"
		},
		meta: [{
				charset: "utf-8"
			},
			{
				name: "viewport",
				content: "width=device-width, initial-scale=1"
			},
			{
				hid: "description",
				name: "description",
				content: ""
			}
		],
		link: [
			{
				rel: "icon",
				type: "image/x-icon",
				href: "/favicon.ico"
			},
			{
				rel: "stylesheet",
				href: "https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,400;0,700;1,400;1,700&display=swap"
			}
		]
	},
	css: [],
	plugins: [],
	components: true,
	modules: ["@nuxtjs/style-resources", "@nuxtjs/axios"],
	styleResources: {
		scss: ["./assets/style/style.scss"]
	},
	build: {}
}